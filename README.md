# testLang(s)

A collection of MAL languages specifically designed to test
software working with MAL.

To compile a MAL language and generate the corresponding JAR file we
just need to issue:

```
mvn package -PsecuriCAD
```

We can also create ".mar" archives by doing:
```
malc /path/to/*.mal
```


Summary of different TestLangs

- testLang1, for basic features (and/or step within same asset)
- testLang2, for attack steps with many dots
- testLang3, for let statements 
- testLang4, for Transitive operator
- testLang5, for Existence and non Existence operators
- testLang6, for +> operator and inheritance
