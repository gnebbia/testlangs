/*
 * Copyright 2020-2021 Foreseeti AB <https://foreseeti.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#id: "org.mal-lang.testlang2"
#version: "1.0.0"

category General {
  asset AA {
    | a0
      -> a1
    | a1
      -> b.b3,
         b.b4
    | a2
      -> b.b1
    | a3
      -> b.c.c2
    | a4
      -> b.c.d.d1
    | a5and
      -> b.c.d.d3and
    | a6
      -> b.c.c1
    | a7
      -> b.c.d.d5and,
         b.b2
    | a8
      -> b.c.d.d6,
         b.c.d.d7
  }

  asset BB {
    | b1
      -> b4
    | b2
      -> c.c3
    | b3
      -> c.d.d3and
    & b4
  }

  asset CC {
    | c1
      -> c2
    | c2
      -> d.d2
    | c3
      -> d.d5and
  }

  asset DD {
    | d1
      -> d5and
    | d2
      -> d5and
    & d3and
      -> d4
    | d4
    & d5and

    | d6
      -> d8localAnd
    | d7
      -> d8localAnd
    & d8localAnd
  }
}

associations {
  AA [a] * <-- Gen1 --> * [b] BB
  BB [b] * <-- Gen2 --> * [c] CC
  CC [c] * <-- Gen3 --> * [d] DD
}
